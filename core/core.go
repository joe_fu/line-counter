package core

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
)

func GetFiles(dirname string, exts []string) (files []string, err error) {

	var count int

	err = filepath.Walk(dirname, func(path string, info fs.FileInfo, err error) error {
		count++
		fmt.Printf("\r扫描文件...... %v", count)

		if info == nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if len(exts) > 0 {
			for _, v := range exts {
				if strings.HasSuffix(info.Name(), v) {
					files = append(files, path)
					break
				}
			}
		} else {
			files = append(files, path) // 全路径文件名
		}
		return nil
	})

	return files, err
}

func Count(file string) (n int, err error) {

	var ret int

	f, err := os.Open(file)
	if err != nil {
		return 0, err
	}

	var content string
	buf := make([]byte, 1024)
	for {
		len, _ := f.Read(buf)
		if len == 0 {
			break
		}
		content += string(buf)
	}

	lines := strings.Split(content, "\r\n")
	for _, line := range lines {
		if line != "" {
			ret++
		}
	}

	return ret, err
}

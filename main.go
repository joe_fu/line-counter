package main

import (
	"LineCounter/core"
	"flag"
	"fmt"
	"strings"
)

func main() {

	var total int
	var dirname string
	var filter string
	flag.StringVar(&dirname, "d", "./", "目标文件夹路径（默认为当前路径）")
	flag.StringVar(&filter, "f", "", "目标文件后缀名（小写），多个后缀以逗号分割（例：go,cs,java,js,ts）")
	flag.Parse()

	var exts []string

	if filter != "" {
		exts = append(exts, strings.Split(filter, "|")...)
	}

	files, err := core.GetFiles(dirname, exts)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("\r")
		for _, file := range files {
			sum, err := core.Count(file)
			if err != nil {
				fmt.Println(err)
			} else {
				fmt.Printf("%v ------ %v\n", file, sum)
				total += sum
			}
		}
	}

	fmt.Printf("总计（行）： %v\n\n", total)

}



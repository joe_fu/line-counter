Line Counter CLI
==

这是一个用于统计代码行数的Windows命令行应用程序。

## 部署
1. 下载 lc.exe
2. 将其所在目录添加至系统环境变量或者将lc.exe置入"\Windows\System32"目录中

## 使用

在目标文件夹处打开命令行窗口然后执行lc命令即可。  

命令格式：
```bat
lc [-d [path]] [-f [filters]]
  -d string
        目标文件夹路径（默认为当前路径） (default "./")
  -f string
        目标文件后缀名（小写），多个后缀以逗号分割（例：go,cs,java,js,ts）
```

例：  
```
D:\>cd Go\hello
D:\Go\hello>lc -f go
main.go ------ 19
hello.go ------ 14
总计（行）： 33
```